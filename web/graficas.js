/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//funcion para obtener los parametros de la url
function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}


//se obtienen los parametros
var status_0 = parseInt(getUrlVars()["status0"]);
var status_1 = parseInt(getUrlVars()["status1"]);
var status_2 = parseInt(getUrlVars()["status2"]);

var total=status_0+status_1+status_2;

///alert(total);


//funcion de highchart para desplegar la grafica
Highcharts.chart('container', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Procesos de firma en tableta'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
    series: [{
        name: 'porcentaje',
        colorByPoint: true,
        data: [{
            name: 'EJECUCION CORRECTA',
            y: status_1,
            sliced: true,
            selected: true
        }, {
            name: 'EJECUCION INCOMPLETA EN LOG',
            y: status_0
        }, {
            name: 'FIRMA DIGITAL: EL PROCESO TERMINO SIN GENERAR ARCHIVOS FIRMADOS',
            y: status_2
        }
       ]
    }]
});



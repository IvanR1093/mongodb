package conexion;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mongodb.Block;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mongodb.client.MongoDatabase; 
import com.mongodb.MongoClient; 
import com.mongodb.MongoCredential;  
import com.mongodb.client.AggregateIterable;
import org.bson.*;
import com.mongodb.client.FindIterable; 
import com.mongodb.client.MongoCollection; 
import com.mongodb.client.MongoDatabase;  
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import java.util.ArrayList;
import java.util.Arrays;

import java.util.Iterator;
import javax.servlet.RequestDispatcher;
import org.json.JSONObject;

/**
 *
 * @author XMY9460
 */
@WebServlet(name = "consultaDocumentos", urlPatterns = {"/consultaDocumentos"})
public class consultaDocumentos extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
           
           // Se crea el cliente Mongo 
      MongoClient mongo = new MongoClient( "localhost" , 27017 ); 
     // Se crea la credencial admin=usuario, test=db, pass=password
      MongoCredential credential = MongoCredential.createCredential("admin", "test", "pass".toCharArray()); 
       
      System.out.println("Se conecto"+ credential.getUserName());
      
       // se accesa a la base test
      MongoDatabase database = mongo.getDatabase("test"); 
      
       //Crear una coleccion 
      //database.createCollection("micoleccion"); 
      
       // obtiene la coleccion: midocs
      MongoCollection<Document> collection = database.getCollection("midocs");
      
      // crea un objeto iterable con los documentos de la base que cumplen con las  condiciones 
      //dentro del objeto busca los elementos con status=2
      //para filtrar por otro parametro solo cambiar por algun otro field de un proceso "processes.FIELD" y el valor por filtrar
      //processes es el array de procesos 
      //query status 0
     AggregateIterable<Document> iterDoc = collection.aggregate(Arrays.asList(
		new Document("$match", new Document("processes.status", 0)),
        new Document("$unwind", "$processes"),
        new Document("$match", new Document("processes.status", 0))));
     
      // obtiene el iterador
      Iterator it = iterDoc.iterator(); 
       ArrayList<String> documentos = new ArrayList<>();
      
      //se inserta los docuumentos en formato json a la lista de documentos
       iterDoc.forEach(new Block < Document > () {
          
       @Override
     public void apply(final Document document) {
         
         documentos.add(document.toJson());
         
     }
 });
       //query status  1
           AggregateIterable<Document> iterDoc1 = collection.aggregate(Arrays.asList(
		new Document("$match", new Document("processes.status", 1)),
        new Document("$unwind", "$processes"),
        new Document("$match", new Document("processes.status", 1))));
     
      // obtiene el iterador
      Iterator it1 = iterDoc1.iterator(); 
       ArrayList<String> documentos1 = new ArrayList<>();
      
      //se inserta los docuumentos en formato json a la lista de documentos
       iterDoc1.forEach(new Block < Document > () {
          
       @Override
     public void apply(final Document document) {
         
         documentos1.add(document.toJson());
         
     }
 });
       //query status 2
           AggregateIterable<Document> iterDoc2 = collection.aggregate(Arrays.asList(
		new Document("$match", new Document("processes.status", 2)),
        new Document("$unwind", "$processes"),
        new Document("$match", new Document("processes.status", 2))));
     
      // obtiene el iterador
      Iterator it2 = iterDoc.iterator(); 
       ArrayList<String> documentos2 = new ArrayList<>();
      
      //se inserta los docuumentos en formato json a la lista de documentos
       iterDoc2.forEach(new Block < Document > () {
          
       @Override
     public void apply(final Document document) {
         
         documentos2.add(document.toJson());
         
     }
 });
      
      //System.out.println("Credentials ::"+ credential);
        //try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
          //  out.println("Conexion a MongoDB </br>");
           // out.println("Se conecto: "+ credential.getUserName()+"</br>");
           // out.println("DB: "+database.getName()+"</br>");
           // out.println("JSON: "+parametro+"</br></br>");
            
           // out.println("Documentos en la base de datos </br>");
            
            //muestra cada uno de los documentos en la coleccion
         //  while (it.hasNext()) {  
         //documentos.add(it.next().toString());
     // i++; 
      //}
      
      //se envian los datos a la vista
      request.setAttribute("user", credential.getUserName());
      request.setAttribute("db", database.getName());
      request.setAttribute("documentos", documentos);
      request.setAttribute("documentos1", documentos1);
      request.setAttribute("documentos2", documentos2);
      
      request.setAttribute("resultados", documentos.size());
      request.setAttribute("resultados1", documentos1.size());
      request.setAttribute("resultados2", documentos2.size());
      
      request.setAttribute("total", documentos2.size()+documentos1.size()+documentos.size());
      
      
      RequestDispatcher view = request.getRequestDispatcher("consulta.jsp");
      view.forward(request, response);  
     //}
     
       
    }
    
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

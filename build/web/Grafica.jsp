<%-- 
    Document   : Grafica
    Created on : Oct 18, 2018, 1:28:46 PM
    Author     : XMY9460
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
        <title>Grafica</title>
    </head>
    <body>
          <nav class="navbar navbar-dark bg-primary">
  <div class="container-fluid">
    <div class="navbar-header">
        <a class="navbar-brand" href="http://localhost:8080/mongoDB/"><font color="white">Servlet Mongo DB</font></a>
    </div>
    <ul class="nav navbar-nav">
        <li class="active"><a href="#"><font color="white">Graficas</font></a></li>
    </ul>
  </div>
</nav>
        <div id="container" style="min-width: 310px; height: 600px; max-width: 600px; margin: 0 auto"></div>
        <script src="graficas.js"></script>
    </body>
</html>
